from rest_framework.fields import SerializerMethodField
from rest_framework.serializers import ModelSerializer

from atracoes.models import Atracao
from core.models import PontoTuristico
from atracoes.api.serializers import AtracaoSerializer
from enderecos.api.serializers import EnderecoSerializer
from comentarios.api.serializers import ComentarioSerializer
from enderecos.models import Endereco


class PontoTuristicoSerializer(ModelSerializer):
    atracoes = AtracaoSerializer(many=True)
    comentarios = ComentarioSerializer(many=True, read_only=True)
    endereco = EnderecoSerializer()
    descricao_completa = SerializerMethodField()

    class Meta:
        model = PontoTuristico
        fields = ('id', 'nome', 'descricao', 'foto', 'aprovado', 'atracoes',
                  'comentarios', 'avaliacoes', 'endereco', 'descricao_completa', 'descricao_completa_2')
        read_only_fields = ('avaliacoes', )

    def cria_atracoes(self, ponto, atracoes):
        for atracao in atracoes:
            at = Atracao.objects.create(**atracao)
            ponto.atracoes.add(at)

    def create(self, validated_data):
        atracoes = validated_data.get('atracoes', [])
        del validated_data['atracoes']

        endereco = validated_data.get('endereco', [])
        del validated_data['endereco']

        ponto = PontoTuristico.objects.create(**validated_data)
        self.cria_atracoes(ponto, atracoes)

        end = Endereco.objects.create(**endereco)
        ponto.endereco = end

        ponto.save()
        return ponto

    def get_descricao_completa(self, obj):
        return '{nome} - {descricao}'.format(nome=obj.nome, descricao=obj.descricao)
