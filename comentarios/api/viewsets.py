from rest_framework.viewsets import ModelViewSet

from comentarios.api.serializers import ComentarioSerializer
from comentarios.models import Comentario


class ComentariosViewSet(ModelViewSet):

    serializer_class = ComentarioSerializer

    def get_queryset(self):
        return Comentario.objects.all()
