from rest_framework.viewsets import ModelViewSet

from atracoes.api.serializers import AtracaoSerializer
from atracoes.models import Atracao


class AtracaoViewSet(ModelViewSet):

    serializer_class = AtracaoSerializer
    filter_fields = ('id', 'nome', 'descricao')

    def get_queryset(self):
        return Atracao.objects.all()
