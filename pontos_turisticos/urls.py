"""pontos_turisticos URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.conf.urls import include
from django.urls import path
from rest_framework import routers
from django.conf import settings
from django.conf.urls.static import static

from avaliacoes.api.viewsets import AvaliacaoViewSet
from comentarios.api.viewsets import ComentariosViewSet
from core.api.viewsets import PontoTuristicoViewSet
from atracoes.api.viewsets import AtracaoViewSet
from enderecos.api.viewsets import EnderecoViewSet
from rest_framework.authtoken.views import obtain_auth_token
from rest_framework_swagger.views import get_swagger_view


router = routers.DefaultRouter()
router.register(r'pontoturistico', PontoTuristicoViewSet, base_name='PontoTuristico')
router.register(r'atracoes', AtracaoViewSet, base_name='Atracao')
router.register(r'enderecos', EnderecoViewSet, base_name='Endereco')
router.register(r'comentarios', ComentariosViewSet, base_name='Comentario')
router.register(r'avaliacoes', AvaliacaoViewSet, base_name='Avaliacao')

schema_view = get_swagger_view(title='Pontos Turisticos API')

urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
    path('api-token-auth/', obtain_auth_token),
    path('docs/', schema_view)
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
